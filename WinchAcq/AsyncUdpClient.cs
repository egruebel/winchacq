﻿using System;
using System.Net.Sockets;
using System.Net;
using System.Text;

namespace WinchAcq
{
    class AsyncUdpClient
    {
        private UdpClient Client { get; set; }
        private IPEndPoint Endpoint;// { get; set; }
        public event EventHandler DataReceived;

        public AsyncUdpClient(int Port)
        {
            Endpoint = new IPEndPoint(IPAddress.Any, Port);
            Client = new UdpClient(Endpoint);
        }

        public void BeginReceive()
        {
            Client.BeginReceive(new AsyncCallback(AsyncDataCallback), Endpoint);
        }

        private void AsyncDataCallback(IAsyncResult result)
        {
            var bytes = Client.EndReceive(result, ref Endpoint);
            string data = Encoding.ASCII.GetString(bytes);
            if (DataReceived != null)
            {
                DataReceived(this, new DataReceivedEventArgs() {Data = data });
            }
            //continuous loop
            BeginReceive();
        }
    }

    public class DataReceivedEventArgs : EventArgs
    {
        public string Data { get; set; }
    }
}
