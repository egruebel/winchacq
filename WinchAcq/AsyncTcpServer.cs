﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace WinchAcq
{
    class AsyncTcpServer
    {
        private TcpListener listener;
        private Thread listenThread;

        public String DataQueue { get; set; }

        public AsyncTcpServer(int TcpPort)
        {
            this.listener = new TcpListener(IPAddress.Any, TcpPort);
            DataQueue = "Null";
            this.listenThread = new Thread(new ThreadStart(ListenForClients));
            this.listenThread.Start();
        }

        private void ListenForClients()
        {
            this.listener.Start();

            while (true)
            {
                //blocks until a client has connected to the server
                TcpClient client = this.listener.AcceptTcpClient();

                //create a thread to handle communication 
                //with connected client
                Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
                clientThread.Start(client);
            }
        }

        private void HandleClientComm(object client)
        {
            TcpClient tcpClient = (TcpClient)client;
            NetworkStream clientStream = tcpClient.GetStream();
            var str = DataQueue;
            var d = Encoding.Default.GetBytes(str); //conversion string => byte array
            clientStream.Write(d, 0, d.Length); 
            tcpClient.Close();
        }

    
    }
}
