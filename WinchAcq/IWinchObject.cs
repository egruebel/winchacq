﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinchAcq
{
    interface IWinchObject
    {
        byte WinchNo { get; set; }
        int Tension { get; set; }
        int Speed { get; set; }
        int Payout { get; set; }
        DateTime DateTime { get; set; }
    }
}
