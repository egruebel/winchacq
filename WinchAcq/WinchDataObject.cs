﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinchAcq
{
    class WinchDataObject: IWinchObject
    {
        public byte WinchNo { get; set; }
        public DateTime DateTime { get; set; }
        public int Tension { get; set; }
        public int Speed { get; set; }
        public int Payout { get; set; }
        public bool PostedToDatabase { get; set; }

        public WinchDataObject()
        {
            PostedToDatabase = false;
        }

        public bool Parse(string RawData)
        {
            try
            {
                var d = RawData.Split(',');
                WinchNo = byte.Parse(d[0].Substring(3, 1));
                DateTime = DateTime.Now; //because the LCI-90i time sucks
                Tension = int.Parse(d[2]);
                Speed = int.Parse(d[3]);
                Payout = int.Parse(d[4]);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool ChangedFrom(WinchDataObject reference, int jitter)
        {
            if (Tension != reference.Tension)
            {
                //test if within the jitter range
                var high = reference.Tension + jitter;
                var low = reference.Tension - jitter;
                if (Tension > high || Tension < low)
                    return true;
            }
            if (Payout != reference.Payout)
                return true;
            if (Speed != reference.Speed)
                return true;

            return false;
        }
    }
}
