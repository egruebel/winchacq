﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinchAcq
{
    class WinchDataHandler
    {
        public AsyncUdpClient client;
        public AsyncTcpServer server;

        public WinchDataObject ThisWinchDataObject { get; set; }
        public WinchDataObject LastWinchDataObject { get; set; }
        public WinchDataObject LastPostedWinchDataObject { get; set; }
        public int Jitter { get; set; }
        public int RecordCounter { get; set; }

        public WinchDataHandler(int jitter)
        {
            Jitter = jitter;
            RecordCounter = 0;
        }

        public void BeginAcq(int UdpPort, int tcpPort)
        {
            client = new AsyncUdpClient(UdpPort);
            client.DataReceived += client_DataReceived;
            client.BeginReceive();
            server = new AsyncTcpServer(tcpPort); //starts listening for requests
        }

        void client_DataReceived(object sender, EventArgs e)
        {
            var args = (DataReceivedEventArgs)e;
            var wdo = new WinchDataObject();
            if (wdo.Parse(args.Data))
            {
                server.DataQueue = string.Format("{0},{1},{2}", wdo.Tension, wdo.Speed, wdo.Payout);
                ThisWinchDataObject = wdo;
                Evaluate();
            }
        }

        void Evaluate()
        {
            if (LastPostedWinchDataObject == null)
            {
                //This should only happen at startup
                //otherwise the last posted object will never be null
                LastWinchDataObject = ThisWinchDataObject; 
                WriteToDb(ThisWinchDataObject);
                LastPostedWinchDataObject = ThisWinchDataObject;
            }
            if (ThisWinchDataObject.ChangedFrom(LastPostedWinchDataObject,Jitter))
            {
                //check to see how much time has elapsed and insert a point to avoid sloping
                if ((ThisWinchDataObject.DateTime - LastPostedWinchDataObject.DateTime).Seconds >= 12)
                {
                    WriteToDb(GeneratedFillerPoint);
                }
                WriteToDb(ThisWinchDataObject);
                LastPostedWinchDataObject = ThisWinchDataObject;
            }
            else if((ThisWinchDataObject.DateTime - LastPostedWinchDataObject.DateTime).Minutes >= 12)
            {
                //ten minutes has elapsed. Insert a generated point so there's no 10 minute slope
                if (ThisWinchDataObject.Tension != LastPostedWinchDataObject.Tension)
                {
                    WriteToDb(GeneratedFillerPoint);
                }
                WriteToDb(ThisWinchDataObject);
                LastPostedWinchDataObject = ThisWinchDataObject;
            }
            LastWinchDataObject = ThisWinchDataObject;
        }

        void WriteToDb(WinchDataObject val)
        {
            try
            {
                using (var db = new WinchDataClassesDataContext())
                {
                    switch (val.WinchNo)
                    {
                        case 1:
                            var obj1 = new Winch1 { 
                                WinchNo = val.WinchNo,
                                DateTime = val.DateTime,
                                Speed = val.Speed,
                                Payout = val.Payout,
                                Tension = val.Tension
                            };
                            db.Winch1s.InsertOnSubmit(obj1);
                            break;
                        case 2:
                            var obj2 = new Winch2() { 
                                WinchNo = val.WinchNo,
                                DateTime = val.DateTime,
                                Speed = val.Speed,
                                Payout = val.Payout,
                                Tension = val.Tension
                            };
                            db.Winch2s.InsertOnSubmit(obj2);
                            break;
                        case 3:
                            var obj3 = new Winch3() { 
                                WinchNo = val.WinchNo,
                                DateTime = val.DateTime,
                                Speed = val.Speed,
                                Payout = val.Payout,
                                Tension = val.Tension
                            };
                            db.Winch3s.InsertOnSubmit(obj3);
                            break;
                    }
                    db.SubmitChanges();
                    //if you're here then the database insert was successful
                    val.PostedToDatabase = true;
                    RecordCounter++;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database error: ");
                Console.WriteLine(ex.Message);
            }
        }

        private WinchDataObject GeneratedFillerPoint
        {
            get
            {
                //essentially returns the last posted point with the datetime of the last received point
                return new WinchDataObject()
                {
                    WinchNo = LastPostedWinchDataObject.WinchNo,
                    DateTime = LastWinchDataObject.DateTime,
                    Tension = LastPostedWinchDataObject.Tension,
                    Speed = LastPostedWinchDataObject.Speed,
                    Payout = LastPostedWinchDataObject.Payout
                };
            }
        }
    }
}
