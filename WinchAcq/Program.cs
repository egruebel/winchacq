﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace WinchAcq
{
    class Program
    {
        //args[0] = UdpPort
        //args[1] = TcpServicePort
        //args[2] = TensionJitter
        static void Main(string[] args)
        {
            if (args.Count() < 3)
                PrintHelp();
            //parse the supplied arguments
            int udpPort = 0, tcpPort = 0, jitter = 0;
            try
            {
                udpPort = int.Parse(args[0]);
                tcpPort = int.Parse(args[1]);
                jitter = int.Parse(args[2]);
            }
            catch
            {
                Console.WriteLine("Invalid argument!!!");
                PrintHelp();
            }
            //if you made it here the arguments are valid. Good for you!
            //Now lets make sure that the database as accessible
            for (int trys = 1; trys < 4; trys++)
            {
                using (var db = new WinchDataClassesDataContext())
                {
                    if (!db.DatabaseExists())
                    {
                        System.Threading.Thread.Sleep(1000 * 30); //wait 30 seconds for the PC to start the SQL server instance
                        if(trys == 3)
                        {
                            Console.WriteLine("Unable to connect to database!!!");
                            Console.WriteLine(ConfigurationManager.ConnectionStrings["WinchAcq.Properties.Settings.dbWinchDatabaseConnectionString"]);
                            Environment.Exit(1);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Connected to database. Starting acquisition.");
                        
                        trys = 4;
                    }
                }
            }
            //arguments are okay and database is all set    
            //start acquisition
            var receiver = new WinchDataHandler(jitter);
            receiver.BeginAcq(udpPort,tcpPort);
            Console.WriteLine("Allowable tension jitter " + receiver.Jitter);
            Console.WriteLine("Opening UDP port " + udpPort.ToString());
            Console.WriteLine("Opening TCP port " + tcpPort.ToString());
            int logTracker = 0;
            while (true)
            {
                System.Threading.Thread.Sleep(1000);
                logTracker++;
                if (logTracker == 60 * 60 * 6 || logTracker == 2)
                {
                    logTracker = 2;
                    Console.WriteLine(string.Format("{0} --- {1} records inserted since last report. ", DateTime.Now, receiver.RecordCounter));
                    receiver.RecordCounter = 0;
                }
            }
        }


        static void PrintHelp()
        {
            Console.WriteLine("This application takes four parameters");
            Console.WriteLine("---------------------------------------------");
            Console.WriteLine("1) UDP port number to listen for winch data");
            Console.WriteLine("2) TCP port to open for realtime data service");
            Console.WriteLine("1) Allowable tension jitter (lbs)");
            Console.WriteLine("1) Start delay in seconds");
            Console.WriteLine("");
            Console.WriteLine("Press any key to exit");
            Console.ReadLine();
            Environment.Exit(1);
        }
    }
}
